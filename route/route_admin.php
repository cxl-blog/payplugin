<?php

use think\facade\Route;

Route::group('admin', function() {
})->middleware([
\app\index\component\middleware\NeedLoginMiddleware::class,
\app\index\component\middleware\PermissionMiddleware::class
]);