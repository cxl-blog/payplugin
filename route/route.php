<?php

use think\facade\Route;

Route::get('/order/show', 'payment/order.Order/show')->name('order_show');

Route::post('/order/create', 'payment/order.Order/create')->name('order_create');

Route::get("/pay/plugin/index", 'payment/order.Index/index')->name("pay_index");

Route::get('/cashier/show', 'payment/Cashier/show')->name('cashier_show');

Route::rule('/cashier/pay/success', 'payment/Cashier/success')->name('cashier_pay_success');

Route::rule('/cashier/redirect', 'payment/Cashier/redirect')->name('cashier_redirect');

Route::get('/cashier/:sn/price', 'payment/Cashier/price')->name('cashier_pay_price');

Route::get('/cashier/:payment/notify', 'payment/Cashier/notify')->name('cashier_pay_notify');