<?php

namespace app\payment\payment;

class WeChatAppGateway extends WechatGateway
{
    protected function getSetting()
    {
        $config = config('payment.platforms')['wechat_app'];

        return array(
            'appid' => $config['appid'],
            'mch_id' => $config['mch_id'],
            'key' => $config['key'],
            'cert_path' => $config['cert_path'],
            'key_path' => $config['key_path'],
        );
    }
}
