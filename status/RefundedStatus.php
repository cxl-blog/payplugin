<?php

namespace app\payment\status;

use app\payment\service\PayAccountServiceImpl;

class RefundedStatus extends AbstractStatus
{
    const NAME = 'refunded';

    public function getName()
    {
        return self::NAME;
    }

    public function process($data = array())
    {
        $trade = $this->getPayTradeDao()->update(array(
            'status' => self::NAME,
            'refund_success_time' => time(),
        ), ['id' => $this->PayTrade['id']]);

        return $trade;
    }

    protected function getAccountService()
    {
        return new PayAccountServiceImpl();
    }
}
