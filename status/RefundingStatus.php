<?php

namespace app\payment\status;

class RefundingStatus extends AbstractStatus
{
    const NAME = 'refunding';

    public function getName()
    {
        return self::NAME;
    }

    public function refunded($data = array())
    {
        return $this->getPayStatus(RefundedStatus::NAME)->process($data);
    }

    public function process($data = array())
    {
        return $this->getPayTradeDao()->update(array(
            'status' => RefundingStatus::NAME,
        ), ['id' => $this->PayTrade['id']]);
    }
}
