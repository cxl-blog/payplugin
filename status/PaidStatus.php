<?php

namespace app\payment\status;

class PaidStatus extends AbstractStatus
{
    const NAME = 'paid';

    public function getName()
    {
        return self::NAME;
    }

    public function process($data = array())
    {
        return $this->getPayTradeDao()->update([
            'statue' => self::NAME,
            ], ['id' => $this->PayTrade['id']]);
    }

    public function refunding()
    {
        return $this->getPayStatus(RefundingStatus::NAME)->process();
    }

    public function refunded($data=array())
    {
        return $this->getPayStatus(RefundedStatus::NAME)->process($data);
    }
}