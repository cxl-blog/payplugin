<?php

namespace app\payment\status;

class ClosingStatus extends AbstractStatus
{
    const NAME = 'closing';

    public function getName()
    {
        return self::NAME;
    }

    public function process($data = array())
    {
        return $this->getPayTradeDao()->update(array(
            'status' => self::NAME,
        ), ['id' => $this->PayTrade['id']]);
    }

    public function closed()
    {
        return $this->getPayStatus(ClosedStatus::NAME)->process();
    }
}