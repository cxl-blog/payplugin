<?php

namespace app\payment\status;


use app\index\component\exception\AccessDeniedException;
use http\Exception\InvalidArgumentException;
use think\Db;
use think\Exception;

class PayTradeContext
{
    protected $PayTrade;
    protected $status;

    function __construct(){}

    public function setPayTrade($PayTrade)
    {
        $this->PayTrade = $PayTrade;
        $this->status = config("payment.payment_trade_status_{$PayTrade['status']}");

        $this->status->setPayTrade($PayTrade);
    }

    public function getPayTrade()
    {
        return $this->PayTrade;
    }

    public function getStatus()
    {
        return $this->status;
    }

    function __call($method, $arguments)
    {
        $status = $this->getNextStatusName($method);

        if (!method_exists($this->status, $method)) {
            throw new InvalidArgumentException("can't change {$this->PayTrade['status']} to {$status}.");
        }

        try {
            Db::startTrans();
            $orderRefund = call_user_func_array(array($this->status, $method), $arguments);
            Db::commit();
        } catch (AccessDeniedException $e) {
            Db::rollback();
            throw $e;
        } catch (InvalidArgumentException $e) {
            Db::rollback();
            throw $e;
        } catch (Exception $e) {
            Db::rollback();
            throw $e;
        } catch (\Exception $e) {
            Db::rollback();
            throw new Exception($e->getMessage());
        }

//        $this->dispatch("payment_trade.{$status}", $orderRefund);
        return $orderRefund;
    }

    private function getNextStatusName($method)
    {
        return $this->humpToLine($method);
    }

    private function humpToLine($str){
        $str = preg_replace_callback('/([A-Z]{1})/',function($matches){
            return '_'.strtolower($matches[0]);
        },$str);

        if (strpos($str , '_') === 0) {
            return substr($str,1,strlen($str));
        }

        return $str;
    }

//    private function getDispatcher()
//    {
//        return $this->biz['dispatcher'];
//    }
//
//    protected function dispatch($eventName, $subject, $arguments = array())
//    {
//        if ($subject instanceof GenericEvent) {
//            $event = $subject;
//        } else {
//            $event = new GenericEvent($subject, $arguments);
//        }
//
//        return $this->getDispatcher()->dispatch($eventName, $event);
//    }

}
