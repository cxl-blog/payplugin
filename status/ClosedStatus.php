<?php

namespace app\payment\status;

use app\payment\service\PayAccountServiceImpl;

class ClosedStatus extends AbstractStatus
{
    const NAME = 'closed';

    public function getName()
    {
        return self::NAME;
    }

    public function process($data = array())
    {
        return $this->getPayTradeDao()->update(array(
            'status' => ClosedStatus::NAME,
        ), ['id' => $this->PayTrade['id']]);
//        return $this->getAccountService()->releaseCoin($trade['user_id'], $trade['coin_amount']);
    }

    protected function getAccountService()
    {
        return new PayAccountServiceImpl();
    }
}