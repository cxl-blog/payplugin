<?php

namespace app\payment\status;

use app\index\component\exception\AccessDeniedException;
use app\payment\model\PayTradeModel;

abstract class AbstractStatus
{
    protected $PayTrade;

    public function setPayTrade($PayTrade)
    {
        $this->PayTrade = $PayTrade;
    }

    function __construct(){}

    abstract public function getName();

    public function process($data = array())
    {
        throw new AccessDeniedException('can not change status to '.$this->getName());
    }

    public function getPayStatus($name)
    {
        $status = config('payment_trade_status.'.$name);
        $status->setPayTrade($this->PayTrade);
        return $status;
    }

    protected function getPayTradeDao()
    {
        return new PayTradeModel();
    }
}