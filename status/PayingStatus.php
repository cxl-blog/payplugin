<?php

namespace app\payment\status;

class PayingStatus extends AbstractStatus
{
    const NAME = 'paying';

    public function getName()
    {
        return self::NAME;
    }

    public function process($data = array())
    {
        return $this->getPayTradeDao()->update(array(
            'status' => PayingStatus::NAME,
        ), ['id' => $this->PayTrade['id']]);
    }

    public function paid($data)
    {
        return $this->getPayStatus(PaidStatus::NAME)->process();
    }

    public function closing()
    {
        return $this->getPayStatus(ClosingStatus::NAME)->process();
    }
}