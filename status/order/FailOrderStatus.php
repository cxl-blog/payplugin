<?php

namespace app\payment\status\order;

class FailOrderStatus extends AbstractOrderStatus
{
    const NAME = 'fail';

    public function getName()
    {
        return self::NAME;
    }

    public function process($data = array())
    {
        $order = $this->getOrderModel()->update(array(
            'status' => self::NAME,
            'fail_data' => $data
        ), ['id' => $this->order['id']]);

        $items = $this->getOrderItemModel()->findByOrderId($this->order['id']);
        foreach ($items as $item) {
            $this->getOrderItemModel()->update(array(
                'status' => self::NAME,
            ), ['id' => $item['id']]);
        }

        return $order;
    }

    public function success($data = array())
    {
        return $this->getOrderStatus(SuccessOrderStatus::NAME)->process($data);
    }

    public function refunding($data = array())
    {
        return $this->getOrderStatus(RefundingOrderStatus::NAME)->process($data);
    }
}