<?php

namespace app\payment\status\order;

use app\index\common\ArrayToolkit;

class PaidOrderStatus extends AbstractOrderStatus
{
    const NAME = 'paid';

    public function getName()
    {
        return self::NAME;
    }

    public function process($data = array())
    {
        $data = ArrayToolkit::parts($data, array(
            'order_sn',
            'trade_sn',
            'pay_time',
            'payment',
            'paid_cash_amount',
            'paid_coin_amount',
        ));

        $order = $this->getOrderModel()->getBySn($data['order_sn'], array('lock' => true));
        $order = $this->payOrder($order, $data);

        return $order;
    }

    protected function payOrder($order, $data)
    {
        $data = ArrayToolkit::parts($data, array(
            'trade_sn',
            'pay_time',
            'payment',
            'paid_cash_amount',
            'paid_coin_amount',
        ));
        $data['status'] = PaidOrderStatus::NAME;
        $data['refund_deadline'] = empty($order['expired_refund_days']) ? 0 : $data['pay_time'] + $order['expired_refund_days'] * 86400;
        return $this->getOrderModel()->update($data, ['id' => $order['id']]);
    }

    public function success($data = array())
    {
        return $this->getOrderStatus(SuccessOrderStatus::NAME)->process($data);
    }

    public function fail($data = array())
    {
        return $this->getOrderStatus(FailOrderStatus::NAME)->process($data);
    }

    public function refunding($data = array())
    {
        return $this->getOrderStatus(RefundingOrderStatus::NAME)->process($data);
    }
}