<?php

namespace app\payment\status\order;

class SuccessOrderStatus extends AbstractOrderStatus
{
    const NAME = 'success';

    public function getName()
    {
        return self::NAME;
    }

    public function process($data = array())
    {
        $order = $this->getOrderModel()->update(array(
            'status' => self::NAME,
            'success_data' => $data
        ), ['id' => $this->order['id']]);

        $items = $this->getOrderItemModel()->findByOrderId($this->order['id']);
        foreach ($items as $item) {
            $this->getOrderItemModel()->update(array(
                'status' => self::NAME,
            ), ['id' => $item['id']]);
        }

        return $order;
    }

    public function refunding($data = array())
    {
        return $this->getOrderStatus(RefundingOrderStatus::NAME)->process($data);
    }

    public function finished($data = array())
    {
        return $this->getOrderStatus(FinishedOrderStatus::NAME)->process($data);
    }
}