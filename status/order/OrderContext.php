<?php

namespace app\payment\status\order;

use app\index\common\ArrayToolkit;
use app\index\component\exception\AccessDeniedException;
use app\index\component\exception\ModelException;
use MongoDB\Driver\Exception\RuntimeException;
use app\payment\model\order\OrderLogModel;
use app\payment\model\order\OrderModel;
use app\payment\model\order\WorkFlowService;
use app\payment\status\OrderStatusCallback;
use think\Container;
use think\Db;
use think\Exception;

class OrderContext
{
    protected $order;
    /**
     * @var AbstractOrderStatus
     */
    protected $status;

    function __construct($biz)
    {
    }

    public function setOrder($order)
    {
        $this->order = $order;
        $this->status = Container::get("order_status.{$order['status']}");

        $this->status->setOrder($order);
    }

    public function getOrder()
    {
        return $this->order;
    }

    public function getStatus()
    {
        return $this->status;
    }

    public function created($data)
    {
        $this->status = Container::get("order_status.created");

        try {
            Db::startTrans();
            $order = $this->status->process($data);
            Db::commit();
        } catch (Exception $e) {
            Db::rollback();
            throw $e;
        } catch (\InvalidArgumentException $e) {
            Db::rollback();
            throw $e;
        } catch (RuntimeException $e) {
            Db::rollback();
            throw $e;
        } catch (\Exception $e) {
            Db::rollback();
            throw new ModelException($e->getMessage());
        }

        $this->createOrderLog($order);

        return $order;
    }

    function __call($method, $arguments)
    {
        $status = $this->getNextStatusName($method);

        if (!method_exists($this->status, $method)) {
            throw new AccessDeniedException("can't change {$this->order['status']} to {$status}.");
        }

        try {
            Db::startTrans();
            $order = call_user_func_array(array($this->status, $method), $arguments);
            Db::commit();
        } catch (AccessDeniedException $e) {
            Db::rollback();
            throw $e;
        } catch (\InvalidArgumentException $e) {
            Db::rollback();
            throw $e;
        } catch (Exception $e) {
            Db::rollback();
            throw $e;
        } catch (\Exception $e) {
            Db::rollback();
            throw new ModelException($e->getMessage());
        }

        $this->createOrderLog($order);

        return $order;
    }

    public function onOrderStatusChange($status, $order)
    {
        $orderItems = $order['items'];
        unset($order['items']);
        unset($order['deducts']);

        $results = array();
        $method = 'on' . ucfirst($status);

        foreach ($orderItems as $orderItem) {
            $orderItem['order'] = $order;

            $processor = $this->getProductCallback($orderItem);
            if (!empty($processor) && $processor instanceof OrderStatusCallback && method_exists($processor, $method)) {
                $results[] = $processor->$method($orderItem);
            }
        }

        $results = array_unique($results);
        if ($status == PaidOrderStatus::NAME) {
            if (in_array(OrderStatusCallback::SUCCESS, $results) && count($results) == 1) {
                $this->getWorkflowService()->finish($order['id']);
                if ($order['refund_deadline'] == 0) {
                    $this->getWorkflowService()->finished($order['id']);
                }
            } else if (count($results) > 0) {
                $this->getWorkflowService()->fail($order['id']);
            }
        }
    }

    protected function getProductCallback($orderItem)
    {
        if ((new Container())->has("order_product.{$orderItem['target_type']}")) {
            return null;
        }
        return Container::get("order_product.{$orderItem['target_type']}");
    }

    private function getNextStatusName($method)
    {
        return $this->humpToLine($method);
    }

    private function humpToLine($str)
    {
        $str = preg_replace_callback('/([A-Z]{1})/', function ($matches) {
            return '_' . strtolower($matches[0]);
        }, $str);

        if (strpos($str, '_') === 0) {
            return substr($str, 1, strlen($str));
        }

        return $str;
    }

    protected function createOrderLog($order, $dealData = array())
    {
        $orderLog = array(
            'status' => 'order.' . $order['status'],
            'order_id' => $order['id'],
            'user_id' => current_user()['id'],
            'deal_data' => $dealData,
            'ip' => request()->ip(),
        );
        return $this->getOrderLogModel()->create($orderLog, true);
    }

    protected function getOrderLogModel()
    {
        return new OrderLogModel();
    }

    protected function getOrderModel()
    {
        return new OrderModel();
    }

    /**
     * @return WorkFlowService
     */
    protected function getWorkflowService()
    {
        return new WorkFlowService();
    }
}
