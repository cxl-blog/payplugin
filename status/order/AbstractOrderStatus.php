<?php

namespace app\payment\status\order;

use app\payment\exception\OrderStatusException;
use app\payment\model\order\OrderItemModel;
use app\payment\model\order\OrderModel;
use think\Container;

abstract class AbstractOrderStatus implements OrderStatus
{
    abstract public function getName();

    abstract public function process($data);

    protected $order;

    function __construct()
    {
    }

    public function setOrder($order)
    {
        $this->order = $order;
    }

    public function start($order, $orderItems)
    {
        throw new OrderStatusException('can not start order.');
    }

    public function paying($data = array())
    {
        throw new OrderStatusException("can not change order #{$this->order['id']} status to paying.");
    }

    public function paid($data = array())
    {
        throw new OrderStatusException("can not change order #{$this->order['id']} status to paid.");
    }

    public function closed($data = array())
    {
        throw new OrderStatusException("can not change order #{$this->order['id']} status to closed.");
    }

    public function success($data = array())
    {
        throw new OrderStatusException("can not change order #{$this->order['id']} status to success.");
    }

    public function fail($data = array())
    {
        throw new OrderStatusException("can not change order #{$this->order['id']} status to fail.");
    }

    public function finished($data = array())
    {
        throw new OrderStatusException("can not change order #{$this->order['id']} status to finished.");
    }

    protected function getOrderStatus($name)
    {
        $orderStatus = Container::get('order_status.' . $name);
        $orderStatus->setOrder($this->order);
        return $orderStatus;
    }

    protected function changeStatus($name)
    {
        $order = $this->getOrderModel()->update(array(
            'status' => $name,
        ), ['id' => $this->order['id']]);

        $items = $this->getOrderItemModel()->findByOrderId($this->order['id']);
        foreach ($items as $item) {
            $this->getOrderItemModel()->update(array(
                'status' => $name,
            ), ['id' => $item['id']]);
        }

        return $order;
    }

    protected function getOrderModel()
    {
        return new OrderModel();
    }

    protected function getOrderItemModel()
    {
        return new OrderItemModel();
    }
}