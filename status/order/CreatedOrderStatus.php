<?php

namespace app\payment\status\order;


use app\index\common\ArrayToolkit;
use http\Exception\InvalidArgumentException;

class CreatedOrderStatus extends AbstractOrderStatus
{
    const NAME = 'created';

    public function getName()
    {
        return self::NAME;
    }

    public function process($data)
    {
        $orderItems = $this->validateFields($data['order'], $data['orderItems']);
        $order = ArrayToolkit::parts($data['order'], array(
            'title',
            'callback',
            'source',
            'user_id',
            'created_reason',
            'seller_id',
            'price_type',
            'deducts',
            'create_extra',
            'device',
            'expired_refund_days'
        ));

        return $order;
    }

    public function closed($data = array())
    {
        return $this->getOrderStatus(ClosedOrderStatus::NAME)->process($data);
    }

    public function paid($data = array())
    {
        return $this->getOrderStatus(PaidOrderStatus::NAME)->process($data);
    }

    public function paying($data = array())
    {
        return $this->getOrderStatus(PayingOrderStatus::NAME)->process($data);
    }

    protected function validateFields($order, $orderItems)
    {
        if (!ArrayToolkit::requireds($order, array('user_id'))) {
            throw new InvalidArgumentException('user_id is required in order.');
        }

        foreach ($orderItems as $item) {
            if (!ArrayToolkit::requireds($item, array(
                'title',
                'price_amount',
                'target_id',
                'target_type'))) {
                throw new InvalidArgumentException('args is invalid.');
            }
        }

        return $orderItems;
    }

    protected function saveOrder($data)
    {
        $order = $data['order'];
        $orderDeducts = $data['orderDeducts'];
        $items = $data['orderItems'];

        $user = current_user();
        $order['sn'] = $this->generateSn();
        $order['price_amount'] = $this->countOrderPriceAmount($items);
        $order['price_type'] = empty($order['price_type']) ? 'money' : $order['price_type'];
        $order['pay_amount'] = $this->countOrderPayAmount($order['price_amount'], $orderDeducts, $items);
        $order['created_user_id'] = $user['id'];
        $order = $this->getOrderModel()->createOrder($order);
        return $order;
    }

    protected function countOrderPriceAmount($items)
    {
        $priceAmount = 0;
        foreach ($items as $item) {
            $priceAmount = $priceAmount + $item['price_amount'];
        }
        return $priceAmount;
    }

    // TODO: 不暴露方法
    public static function countOrderPayAmount($payAmount, $orderDeducts, $items)
    {
        foreach ($orderDeducts as $deduct) {
            $payAmount = $payAmount - $deduct['deduct_amount'];
        }

        foreach ($items as $item) {
            if (empty($item['deducts'])) {
                continue;
            }

            foreach ($item['deducts'] as $deduct) {
                $payAmount = $payAmount - $deduct['deduct_amount'];
            }
        }

        if ($payAmount<0) {
            $payAmount = 0;
        }

        return $payAmount;
    }

    protected function generateSn()
    {
        return date('YmdHis', time()).mt_rand(10000, 99999);
    }

    protected function createOrderItems($order, $items)
    {
        $savedItems = array();
        foreach ($items as $item) {
            $deducts = array();
            if (!empty($item['deducts'])) {
                $deducts = $item['deducts'];
                unset($item['deducts']);
            }
            $item['order_id'] = $order['id'];
            $item['seller_id'] = $order['seller_id'];
            $item['user_id'] = $order['user_id'];
            $item['sn'] = $this->generateSn();
            $item['pay_amount'] = $this->countOrderItemPayAmount($item, $deducts);
            $item = $this->getOrderItemModel()->create($item);
            $savedItems[] = $item;
        }

        $order['items'] = $savedItems;
        return $order;
    }

    protected function countOrderItemPayAmount($item, $deducts)
    {
        $priceAmount = $item['price_amount'];

        foreach ($deducts as $deduct) {
            $priceAmount = $priceAmount - $deduct['deduct_amount'];
        }

        if ($priceAmount < 0) {
            $priceAmount = 0;
        }

        return $priceAmount;
    }
}