<?php

namespace app\payment\status\order;

use app\payment\service\PayServiceImpl;

class ClosedOrderStatus extends AbstractOrderStatus
{
    const NAME = 'closed';

    public function getName()
    {
        return self::NAME;
    }

    public function process($data = array())
    {
        $closeTime = time();
        $order = $this->getOrderModel()->update(array(
            'status' => ClosedOrderStatus::NAME,
            'close_time' => $closeTime,
        ), ['id' => $this->order['id']]);

//        $items = $this->getOrderItemModel()->findByOrderId($this->order['id']);
//        foreach ($items as $key => $item) {
//            $items[$key] = $this->getOrderItemModel()->update(array(
//                'status' => ClosedOrderStatus::NAME,
//                'close_time' => $closeTime
//            ), ['id' => $item['id']]);
//        }

        $this->getPayService()->closeTradesByOrderSn($order['sn']);

        return $order;
    }

    protected function getPayService()
    {
        return new PayServiceImpl();
    }
}