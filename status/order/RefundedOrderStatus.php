<?php

namespace app\payment\status\order;

class RefundedOrderStatus extends AbstractOrderStatus
{
    const NAME = 'refunded';

    public function getName()
    {
        return self::NAME;
    }

    public function process($data = array())
    {
        return $this->changeStatus(self::NAME);
    }

    protected function changeStatus($name)
    {
        $order = $this->getOrderModel()->update(array(
            'status' => $name,
        ), ['id' => $this->order['id']]);

        $items = $this->getOrderItemModel()->findByOrderId($this->order['id']);
        foreach ($items as $item) {
            $this->getOrderItemModel()->update(array(
                'status' => $name,
            ), ['id' => $item['id']]);
        }

        return $order;
    }
}