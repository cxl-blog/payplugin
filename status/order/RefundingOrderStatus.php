<?php

namespace app\payment\status\order;

class RefundingOrderStatus extends AbstractOrderStatus
{
    const NAME = 'refunding';

    public function getName()
    {
        return self::NAME;
    }

    public function process($data = array())
    {
        $order = $this->changeStatus(self::NAME);
        if(empty($order['trade_sn'])) {
            return $order;
        }

        return $order;
    }

    protected function changeStatus($name)
    {
        $order = $this->getOrderModel()->update(array(
            'status' => $name,
        ), ['id' => $this->order['id']]);

        $items = $this->getOrderItemModel()->findByOrderId($this->order['id']);
        foreach ($items as $item) {
            $this->getOrderItemModel()->update(array(
                'status' => $name,
            ), ['id' => $item['id']]);
        }

        return $order;
    }

    public function refunded($data = array())
    {
        return $this->getOrderStatus(RefundedOrderStatus::NAME)->process($data);
    }

    public function success($data = array())
    {
        return $this->getOrderStatus(SuccessOrderStatus::NAME)->process($data);
    }
}