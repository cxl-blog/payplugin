<?php
/**
 * Created by PhpStorm.
 * User: cxl
 * Date: 19-4-24
 * Time: 上午10:57
 */

namespace app\payment\validate;


use app\common\validate\BaseValidate;

class OrderItemValidate extends BaseValidate
{
    protected $field = [
        'title' => '商品名称',
        'detail' => '商品描述',
        'sn' => '编号',
        'order_id' => '商品编号',
        'price_amount' => '商品总价格',
        'pay_amount' => '商品应付价格',
    ];

    protected $rule = [
        'title' => 'esChsAlphaNumDash',
        'sn' => 'number',
//        'price_amount' => 'integer',
//        'pay_amount' => 'integer'
    ];

    public function sceneCreate()
    {
        return $this->append('title', 'require')
            ->append('sn', 'require')
            ->append('user_id', 'require')
            ->append('price_amount', 'require')
            ->append('pay_amount', 'require');
    }
}