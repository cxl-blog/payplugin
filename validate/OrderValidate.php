<?php

namespace app\payment\validate;

use app\common\validate\BaseValidate;

class OrderValidate extends BaseValidate
{
    protected $field = [
        'title' => '订单名称',
        'sn' => '订单号',
        'user_id' => '用户id',
        'price_amount'=> '商品总价格',
        'pay_amount' => '订单价格',
    ];

    protected $rule = [
        'title' => 'esChsAlphaNumDash',
        'sn' => 'number',
//        'price_amount' => 'integer',
//        'pay_amount' => 'integer'
    ];

    public function sceneCreate()
    {
        return $this->append('title', 'require')
            ->append('sn', 'require')
            ->append('user_id', 'require')
            ->append('price_amount', 'require')
            ->append('pay_amount', 'require');
    }
}