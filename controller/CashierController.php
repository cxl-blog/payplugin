<?php

namespace app\payment\controller;

use app\index\common\MathToolkit;
use app\index\controller\DefaultController;
use app\payment\model\order\facade\Currency;
use app\payment\model\order\OrderFacadeService;
use app\payment\model\order\OrderItemModel;
use app\payment\model\order\OrderModel;
use app\payment\model\order\WorkFlowService;
use app\payment\service\PayServiceImpl;
use app\payment\status\order\CreatedOrderStatus;
use app\payment\status\PayingStatus;
use think\facade\Request;

class CashierController extends DefaultController
{
    public function showAction()
    {
        $sn = $this->request->get('sn');

        $order = $this->getOrderModel()->getBySn($sn);

        if (!$order || $this->getCurrentUser()->getId() !== $order['user_id']) {
            throw $this->createNotFoundException();
        }

        if ($this->getOrderFacadeService()->isOrderPaid($order['id'])) {
            return $this->purchaseSuccessAction($order['sn']);
        }

        if (!in_array($order['status'], array(CreatedOrderStatus::NAME, PayingStatus::NAME))) {
            $this->message = '订单状态已改变';
            $this->code = '13';
            return $this->restApiResponse();
        }

//        $payments = $this->getPayService()->findEnabledPayments();

        return $this->restApiResponse(array(
            'order' => $order,
//            'product' => $this->getProduct($order['id']),
//            'payments' => $payments,
        ));
    }

    private function getProduct($orderId)
    {
        $orderItems = $this->getOrderItemModel()->findByOrderId($orderId);
        $orderItem = reset($orderItems);

        return $this->getOrderFacadeService()->getOrderProductByOrderItem($orderItem);
    }

    public function redirectAction()
    {
        $tradeSn = $this->request->get('tradeSn');
        $trade = $this->getPayService()->getTradeByTradeSn($tradeSn);

        if ($trade['user_id'] !== $this->getCurrentUser()->getId()) {
            throw $this->createAccessDeniedException();
        }

        return $this->redirect($trade['platform_created_result']['url']);
    }

    public function successAction(Request $request)
    {
        $tradeSn = $request->get('trade_sn');
        $trade = $this->getPayService()->getTradeByTradeSn($tradeSn);

        $function = "{$trade['type']}SuccessAction";

        return $function($trade);
    }

    public function rechargeSuccessAction($trade)
    {
//        return $this->render('cashier/success.html.twig', array(
//            'goto' => $this->generateUrl('my_coin'),
//        ));
    }

    public function purchaseSuccessAction($trade)
    {
        $order = $this->getOrderModel()->getBySn($trade['order_sn']);

//        $items = $this->getOrderItemModel()->findByOrderId($order['id']);
//        $item1 = reset($items);
//
//        $params = array(
//            'targetId' => $item1['target_id'],
//            'num' => $item1['num'],
//            'unit' => $item1['unit'],
//        );
//        $product = $this->getOrderFacadeService()->getOrderProduct($item1['target_type'], $params);

        return $this->restApiResponse(['payStatus' => 'paid', 'trade' => 'trade']);
        return $this->render('cashier/success.html.twig', array(
            'goto' => $this->generateUrl($product->successUrl[0], $product->successUrl[1]),
        ));
    }

    public function priceAction(Request $request, $sn)
    {
        $order = $this->getOrderModel()->getBySn($sn);
        $coinAmount = $request->get('coinAmount');
        $priceAmount = $this->getOrderFacadeService()->getTradePayCashAmount(
            $order,
            $coinAmount
        );

        $priceParts = (new Currency())->formatToMajorCurrency($priceAmount);
        unset($priceParts['prefix']);
        unset($priceParts['suffix']);

        $data = implode($priceParts);

        return $this->restApiResponse(['data' => $data]);

        return $this->createJsonResponse(array(
            'data' => $this->get('web.twig.order_extension')->majorCurrency($priceAmount),
        ));
    }

    /**
     * @return PayServiceImpl
     */
    private function getPayService()
    {
        return new PayServiceImpl();
    }

    /**
     * @return OrderFacadeService
     */
    private function getOrderFacadeService()
    {
        return new OrderFacadeService();
    }

    /**
     * @return OrderModel
     */
    private function getOrderModel()
    {
        return new OrderModel;
    }

    private function getWorkflowService()
    {
        return new WorkFlowService();
    }

    protected function getOrderItemModel()
    {
        return new OrderItemModel();
    }

    /**
     * Undocumented function
     *
     * @param [type] $name
     * @param [type] $maxAllowance
     * @param [type] $period
     *
     * @return \Codeages\RateLimiter\RateLimiter
     */
    private function getRateLimiter($name, $maxAllowance, $period)
    {
        $rateLimiter = $this->getBiz()->offsetGet('ratelimiter.factory');

        return $rateLimiter($name, $maxAllowance, $period);
    }
}