<?php

namespace app\payment\controller\order;

use app\index\controller\DefaultController;
use app\payment\model\order\OrderFacadeService;
use app\payment\model\order\OrderModel;

class OrderController extends DefaultController
{
    public function showAction()
    {
        //根据产品创建
        $sn = $this->request->get('sn');

        if (empty($sn)) {
            return $this->createInvalidArgumentException('invalid sn');
        }

        $trade = $this->getOrderModel()->getBySn($sn);

        return $this->restApiResponse(['trade' => $trade]);

    }

    public function createAction()
    {
        $fields = $this->request->post();
        $trade = $this->getOrderFacadeService()->createOrder($fields);

        return $this->restApiResponse(['trade' => $trade]);

    }

    public function priceAction()
    {
        //价格换算
    }

    protected function getOrderFacadeService()
    {
        return new OrderFacadeService();
    }

    /**
     * @return OrderModel
     */
    protected function getOrderModel()
    {
        return model('payment/order/OrderModel');
    }
}