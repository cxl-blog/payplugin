<?php

namespace app\payment\controller\order;

use app\index\common\CurlToolkit;
use app\index\component\app\payment\Wxpay\JsApiPay;
use think\facade\Session;
use think\Request;

class WechatController extends PaymentController
{
    public function wechatJsPayAction()
    {
        if ($this->request->get('orderSn')) {
           Session::set('wechat_pay_params', $this->request->request());
        }

        $user = $this->getCurrentUser();

        if (!$user->isLogin()) {
            return $this->createMessageResponse('error', '用户未登录，支付失败。');
        }
// 由用户code 获取openid
//        try {
//
//            $options = config('payment.platforms')['wechat'];
//
//            $jsApi = new JsApiPay(array(
//                'appid' => $options['appid'],
//                'account' => $options['mch_id'],
//                'key' => $options['key'],
//                'secret' => $options['secret'],
//                'redirect_uri' => url('cashier_wechat_js_pay', array()),
//                'isMicroMessenger' => true,
//            ), $this->request);
//
//            $openid = $jsApi->getOpenid();
//        } catch (\Exception $e) {
//            return $this->createMessageResponse('error', '不能使用微信支付，可能是管理员未开启微信支付或配置不正确');
//        }

        $params = Session::get('wechat_pay_params');

//        $apiKernel = $this->get('api_resource_kernel');

        $result = CurlToolkit::sendRequest(
            'POST',
            url('api_trades_add'),
            array(
                'gateway' => 'WechatPay_Js',
                'type' => 'purchase',
                'openid' => 'oH6wh1boa21BlRP94YVQb1v2lFMI',
                'orderSn' => $params['orderSn'],
                'coinAmount' => empty($params['coinAmount']) ? 0 : $params['coinAmount'],
                'payPassword' => empty($params['payPassword']) ? '' : $params['payPassword'],
            )
        );

        if (!empty($result['paidSuccessUrl'])) {
            return redirect($result['paidSuccessUrl']);
        }

        $trade = $this->getPayService()->queryTradeFromPlatform($result['tradeSn']);

        return $this->restApiResponse(['trade' => $trade]);
    }

    public function wechatAppMwebTradeAction()
    {
        $tradeSn = $this->request->get('tradeSn');
        $trade = $this->getPayService()->getTradeByTradeSn($tradeSn);

        if ($trade['status'] == 'created' || $trade['status'] == 'paying') {
            $platformCreatedResult = $this->getPayService()->getCreateTradeResultByTradeSnFromPlatform($tradeSn);

            //app跳转
        }

        // 支付结果
    }

    public function returnAction()
    {
        $tradeSn = $this->request->get('tradeSn');
        $trade = $this->getPayService()->queryTradeFromPlatform($tradeSn);

        return redirect(url('cashier_pay_success', array('trade_sn' => $trade['trade_sn'])));
    }

    public function notifyAction($payment)
    {
        $payment = $this->request->get('payment');
        $result = $this->getPayService()->notifyPaid($payment, $this->request->getContent());

        return $this->restApiResponse($result);
    }
}
