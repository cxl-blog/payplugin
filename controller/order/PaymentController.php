<?php

namespace app\payment\controller\order;

use app\index\controller\DefaultController;
//use Biz\OrderFacade\Service\OrderFacadeService;
use app\payment\service\PayServiceImpl;
use Symfony\Component\HttpFoundation\Request;

abstract class PaymentController extends DefaultController
{
    protected $deviceDetector;

    protected function isMicroMessenger()
    {
        return strpos($this->request->header('User-Agent'), 'MicroMessenger') !== false;
    }

//    /**
//     * @return OrderFacadeService
//     */
//    protected function getOrderFacadeService()
//    {
//        return $this->createService('OrderFacade:OrderFacadeService');
//    }

    /**
     * @return PayServiceImpl
     */
    protected function getPayService()
    {
        return new PayServiceImpl();
    }

    abstract public function notifyAction($payment);
}
