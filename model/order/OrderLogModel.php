<?php

namespace app\payment\model\order;

use app\common\model\DefaultModel;

class OrderLogModel extends DefaultModel
{
    protected $table = 'order_log';

    protected $pk = 'id';

    protected $createTime = 'created_time';

    protected $updateTime = 'updated_time';

    protected $autoWriteTimestamp = true;

    protected $insert = ['ip'];

    protected $type = [
        'deal_data' => 'json'
    ];

    protected function setIpAttr()
    {
        return request()->ip();
    }

    public function findOrderLogsByOrderId($orderId)
    {
        return self::where('order_id', $orderId)->select()->toArray();
    }

    public function declares()
    {
        // TODO: Implement declares() method.
    }
}