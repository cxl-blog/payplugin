<?php

namespace app\payment\model\order\facade;

class CoinCurrency
{
    const ISO_CODE = 'COIN';

    const SYMBOL = '';

    const PREFIX = '';

    const SUFFIX = '虚拟币';

    const EXCHANGE_RATE = 1;

    const PRECISION = 2;

    const DECIMAL_DELIMITER = '.';

    const THOUSAND_DELIMITER = '';
}
