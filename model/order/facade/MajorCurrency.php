<?php

namespace app\payment\model\order\facade;

class MajorCurrency
{
    const ISO_CODE = 'CNY';

    const SYMBOL = '￥';

    const PREFIX = '￥';

    const SUFFIX = '';

    const EXCHANGE_RATE = 1;

    const PRECISION = 2;

    const DECIMAL_DELIMITER = '.';

    const THOUSAND_DELIMITER = '';
}
