<?php

namespace app\payment\model\order;

use app\common\model\DefaultModel;
use app\common\model\system\log\LoggerConstant;
use app\common\model\system\log\LogModel;
use app\index\common\MathToolkit;
use app\index\common\SettingToolkit;
use app\index\component\exception\ModelException;
use app\payment\model\order\facade\Currency;
use app\payment\model\order\facade\Product;
use app\payment\status\order\FailOrderStatus;
use app\payment\status\order\FinishedOrderStatus;
use app\payment\status\order\SuccessOrderStatus;
use app\payment\status\PaidStatus;
use think\Container;


class OrderFacadeService extends DefaultModel
{
    const DEDUCT_TYPE_ADJUST = 'adjust_price';

    private $deductTypeName = array('discount' => '打折', 'coupon' => '优惠券', 'paidCourse' => '班级课程抵扣', 'adjust_price' => '改价');

    public function createOrder($fields)
    {
        $user = current_user();
        /* @var $currency Currency */
        $currency = $this->getCurrency();
        $orderFields = array(
            'user_id' => $user['id'],
            'created_reason' => 'site.join_by_purchase',
            'price_type' => 'CNY',
            'currency_exchange_rate' => $currency->exchangeRate,
            'expired_refund_days' => $this->getRefundDays(),
            'sn' => $this->generateSn()
        );
        $orderFields = array_merge($orderFields, $fields);
        $order = $this->getOrderModel()->createOrder($orderFields);

//        $orderItems = $this->makeOrderItems($product);

//        $order = $this->getWorkflowService()->start($orderFields, $orderItems);

        return $order;
    }

    public function generateSn()
    {
        return date('YmdHis', time()).mt_rand(10000, 99999);
    }

    public function getRefundDays()
    {
//        $refundSetting = $this->getSettingService()->get('refund');
        $refundSetting = SettingToolkit::getSetting('refund');

        return empty($refundSetting['maxRefundDays']) ? 0 : $refundSetting['maxRefundDays'];
    }

    public function isOrderPaid($orderId)
    {
        if ($order = $this->getOrderModel()->getOrder($orderId)) {
            return in_array($order['status'], array(
                SuccessOrderStatus::NAME,
                PaidStatus::NAME,
                FailOrderStatus::NAME,
                FinishedOrderStatus::NAME,
            ));
        } else {
            return false;
        }
    }

    private function makeOrderItems(Product $product)
    {
        $orderItem = array(
            'target_id' => $product->targetId,
            'target_type' => $product->targetType,
            'price_amount' => $product->originPrice,
            'pay_amount' => $product->getPayablePrice(),
            'title' => $product->title,
            'num' => $product->num,
            'unit' => $product->unit,
            'create_extra' => $product->getCreateExtra(),
            'snapshot' => $product->getSnapShot(),
        );

        return array($orderItem);
    }

    public function getOrderProduct($targetType, $params)
    {
        if ((new Container)->bound('order_product.test')) {
//        if (!empty($this->biz['order_product.' . $targetType])) {
            /* @var $product Product */
            $product = Container::get('order_product.test');
            $product->init($params);

            return $product;
        } else {
            throw $this->createInvalidArgumentException("The {$targetType} product not found");
        }
    }

    public function getOrderProductByOrderItem($orderItem)
    {
        if ((new Container())->bound('order_product.' . $orderItem['target_type'])) {
            /* @var $product Product */
            $product = Container::get('order.product.' . $orderItem['target_type']);
            $product->init(array(
                'targetId' => $orderItem['target_id'],
                'num' => $orderItem['num'],
                'unit' => $orderItem['unit'],
            ));

            return $product;
        } else {
            throw $this->createInvalidArgumentException("The {$orderItem['target_type']} product not found");
        }
    }

    public function getTradePayCashAmount($order, $coinAmount)
    {
        $orderCoinAmount = $this->getCurrency()->convertToCoin($order['pay_amount'] / 100);

        return $this->getCurrency()->convertToCNY($orderCoinAmount - $coinAmount);
    }

    public function sumOrderItemPayAmount($conditions)
    {
        return $this->getOrderItemModel()->sumPayAmount($conditions);
    }

    public function checkOrderBeforePay($sn, $params)
    {
        $order = $this->getOrderModel()->getBySn($sn);

        if (empty($order)) {
            throw new ModelException('订单不存在', 2004);
        }

        $user = current_user();

        if (!$user->isLogin()) {
            throw new ModelException('你还没有登录', 16);
        }

        if ($order['user_id'] != $user['id']) {
            throw new ModelException('你不是订单拥有者', 2006);
        }

        return $order;
    }

    /**
     * @return Currency
     */
    private function getCurrency()
    {
        return new Currency();
    }

    /**
     * @return OrderModel
     */
    private function getOrderModel()
    {
        return new OrderModel();
    }

    protected function getOrderItemModel()
    {
        return new OrderItemModel();
    }

    protected function getWorkflowService()
    {
        return new WorkFlowService();
    }

    public function declares()
    {
        // TODO: Implement declares() method.
    }
}