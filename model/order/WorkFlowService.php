<?php
/**
 * Created by PhpStorm.
 * User: cxl
 * Date: 19-4-23
 * Time: 下午5:31
 */

namespace app\payment\model\order;


use app\common\model\DefaultModel;
use app\index\common\ArrayToolkit;
use app\index\component\exception\AccessDeniedException;
use app\payment\service\PayServiceImpl;
use app\payment\status\order\AbstractOrderStatus;
use app\payment\status\order\OrderContext;
use think\Container;

class WorkFlowService extends DefaultModel
{
    public function start($order, $orderItems)
    {
        $this->validateLogin();
        $data = array(
            'order' => $order,
            'orderItems' => $orderItems,
        );
        $order = $this->getOrderContext()->created($data);

        if (0 == $order['pay_amount']) {
            $data = array(
                'order_sn' => $order['sn'],
                'pay_time' => time(),
                'payment' => 'none',
            );

            return $this->paid($data);
        }

        return $order;
    }

    protected function validateLogin()
    {
        if (!current_user()->isLogin()) {
            throw new AccessDeniedException('user is not login.');
        }
    }

    public function paying($id, $data = array())
    {
        return $this->getOrderContext($id)->paying($data);
    }

    public function paid($data)
    {
        $order = $this->getOrderModel()->getBySn($data['order_sn']);
        if (empty($order)) {
            return $order;
        }

        return $this->getOrderContext($order['id'])->paid($data);
    }

    public function close($orderId, $data = array())
    {
        return $this->getOrderContext($orderId)->closed($data);
    }

    public function finish($orderId, $data = array())
    {
        return $this->getOrderContext($orderId)->success($data);
    }

    public function fail($orderId, $data = array())
    {
        return $this->getOrderContext($orderId)->fail($data);
    }

    public function finished($orderId, $data = array())
    {
        return $this->getOrderContext($orderId)->finished($data);
    }

    public function closeExpiredOrders()
    {
        $options = $this->biz['order.final_options'];

        $orders = $this->getOrderModel()->search(array(
            'created_time_LT' => time() - $options['closed_expired_time'],
            'statuses' => array('created', 'paying'),
        ), array('id' => 'DESC'), 0, 1000);

        foreach ($orders as $order) {
            $this->close($order['id']);
        }
    }

    public function finishSuccessOrders()
    {
        $orders = $this->getOrderModel()->search(array(
            'refund_deadline_LT' => time(),
            'status' => 'success',
        ), array('id' => 'DESC'), 0, 1000);

        if (empty($orders)) {
            return;
        }

        $orderIds = ArrayToolkit::column($orders, 'id');
    }

    protected function getOrderContext($orderId = 0)
    {
        /**
         * @var AbstractOrderStatus $orderContext
         */
        $orderContext = Container::get('order_context');

        if (0 == $orderId) {
            return $orderContext;
        }

        $order = $this->getOrderModel()->get($orderId);
        if (empty($order)) {
            throw $this->createNotFoundException("order #{$order['id']} is not found");
        }

        $orderContext->setOrder($order);

        return $orderContext;
    }

    /**
     * @return PayServiceImpl
     */
    protected function getPayService()
    {
        return new PayServiceImpl();
    }

    /**
     * @return OrderLogModel
     */
    protected function getOrderLogDao()
    {
        return new OrderLogModel();
    }

    /**
     * @return OrderItemModel
     */
    protected function getOrderItemDao()
    {
        return new OrderItemModel();
    }

    /**
     * @return OrderModel
     */
    protected function getOrderModel()
    {
        return new OrderModel();
    }

    /**
     * @param $order
     * @param $newPayAmount
     * @param $adjustAmount
     */
    private function addAdjustPriceLog($order, $newPayAmount, $adjustAmount)
    {
        $logData = array(
            'title' => $order['title'],
            'orderId' => $order['id'],
            'oldPrice' => $order['pay_amount'],
            'newPrice' => $newPayAmount,
            'adjust_amount' => $adjustAmount,
        );

        $orderLog = array(
            'status' => 'order.adjust_price',
            'order_id' => $order['id'],
            'user_id' => current_user()['id'],
            'deal_data' => $logData,
            'ip' => request()->ip()
        );

        $this->getOrderLogDao()->create($orderLog);
    }

    public function declares()
    {
        // TODO: Implement declares() method.
    }
}