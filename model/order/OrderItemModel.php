<?php

namespace app\payment\model\order;

use app\common\model\DefaultModel;
use app\payment\validate\OrderItemValidate;

class OrderItemModel extends DefaultModel
{
    protected $table = 'order_item';

    protected $pk = 'id';

    protected $createTime = 'created_time';

    protected $updateTime = 'updated_time';

    protected $type = [
        'create_extra' => 'json',
        'snapshot' => 'json',
    ];

    public function createOrderItem($fields)
    {
        $this->validateFields($fields);

        return self::create($fields, true)->getData();
    }

    public function findByOrderId($orderId)
    {
        return self::where('order_id', $orderId)->select()->toArray();
    }

    public function findByOrderIds($orderIds)
    {
        return self::where('order_id', 'in', $orderIds)->select()->toArray();
    }

    public function getOrderItemByOrderIdAndTargetIdAndTargetType($orderId, $targetId, $targetType)
    {
        $result = self::where([
            'order_id' => $orderId,
            'target_type' => $targetType,
            'target_id' => $targetId,
        ])->find();

        if (empty($result)) {
            return [];
        }

        return $result->toArray();
    }

    public function sumPayAmount($conditions)
    {
        $builder = $this->createQueryBuilder($conditions)
            ->field('sum(`pay_amount`)')
            ->find();

        return $builder;
    }

    public function findByConditions($conditions)
    {
        return $this->createQueryBuilder($conditions)->select()->toArray();
    }

    protected function validateFields(&$fields, $scene = '')
    {
        $validate = new OrderItemValidate();

        if (!$validate->scene($scene)->check($fields)) {
            throw $this->createInvalidArgumentException($validate->getError());
        }
    }

    public function declares()
    {
        return array(
            'orderBys' => array(
                'id',
                'created_time',
            ),
            'conditions' => array(
                ['order_id', 'IN', 'order_ids'],
                ['status', '=', 'status'],
                ['status', 'in', 'statuses'],
                ['target_id', 'in', 'target_ids'],
                ['target_id', '=', 'target_id'],
                ['user_id', '=', 'user_id'],
                ['title', 'like', 'title_LIKE'],
                ['target_type', '=', 'target_type'],
                ['created_time', '>=', 'start_time'],
                ['created_time', '<=', 'end_time'],
                ['pay_time', '<', 'pay_time_LT'],
                ['pay_time', '>', 'pay_time_GT'],
                ['pay_amount', '>', 'pay_amount_GT'],
                ['pay_time', '>=', 'pay_time_GE'],
                ['pay_time', '<=', 'pay_time_LE']
            ),
        );
    }
}