<?php

namespace app\payment\model\order;

use app\common\model\DefaultModel;
use app\index\common\MathToolkit;
use app\payment\validate\OrderValidate;

class OrderModel extends DefaultModel
{
    protected $table = 'order';

    protected $pk = 'id';

    protected $createTime = 'created_time';

    protected $updateTime = 'updated_time';

    protected $autoWriteTimestamp = true;

    protected $type = [
        'pay_data' => 'json',
        'callback' => 'json',
        'create_extra' => 'json',
        'success_data' => 'json',
        'fail_data' => 'json',
    ];

    protected function setPriceAmountAttr($value)
    {
        return MathToolkit::simple($value, 100);
    }

    protected function setPayAmountAttr($value)
    {
        return MathToolkit::simple($value, 100);
    }

    protected function getPriceAmountAttr($value)
    {
        return MathToolkit::simple($value, 0.01);
    }

    protected function getPayAmountAttr($value)
    {
        return MathToolkit::simple($value, 0.01);
    }

    public function getOrder($id)
    {
        $result = self::get($id);

        if (empty($result)) {
            return [];
        }

        return $result->toArray();
    }

    public function createOrder($fields)
    {
        $this->validateFields($fields, 'create');

        return self::create($fields, true)->getData();
    }

    public function getBySn($sn, array $options = array())
    {
        $lock = isset($options['lock']) && true === $options['lock'];

        $builder = self::where('sn', $sn);

        if ($lock) {
            $builder->lock(true);
        }

        $result = $builder->find();

        if (empty($result)) {
            return [];
        }

        return $result->toArray();
    }

    public function findByIds(array $ids)
    {
        if (empty($ids)) {
            return [];
        }

        return self::where('id', 'in', $ids)->select()->toArray();
    }

    public function findBySns(array $orderSns)
    {
        if (empty($orderSns)) {
            return [];
        }

        return self::where('sn', 'in', $orderSns)->select()->toArray();
    }

    public function findByInvoiceSn($invoiceSn)
    {

        return self::where('invoice_sn', $invoiceSn)->select()->toArray();
    }

    public function countOrderItem($conditions)
    {
        return $this->createQueryBuilder($conditions)->count();
    }

    public function searchOrderItems($conditions, $orderBys, $start, $limit)
    {
        return $this->search($conditions, $orderBys, $start, $limit);
    }

    public function sumPaidAmount($conditions)
    {
        $builder = $this->createQueryBuilder($conditions)
            ->field('sum(`pay_amount`) as payAmount, sum(`paid_cash_amount`) as cashAmount, sum(`paid_coin_amount`) as coinAmount');

        return $builder->select()->toArray();
    }

    public function sumGroupByDate($column, $conditions, $sort, $dateColumn = 'pay_time')
    {
        if (!$this->isSumColumnAllow($column)) {
            throw $this->createInvalidArgumentException('column is not allowed');
        }

        if (!$this->isDateColumnAllow($dateColumn)) {
            throw $this->createInvalidArgumentException('dateColumn is not allowed');
        }

        if (!in_array(strtoupper($sort), array('ASC', 'DESC'), true)) {
            throw $this->createInvalidArgumentException("SQL order by direction is only allowed `ASC`, `DESC`, but you give `{$sort}`.");
        }

        $builder = $this->createQueryBuilder($conditions)
            ->field("sum({$column}) as count ,from_unixtime({$dateColumn},'%Y-%m-%d') date")
            ->group("date", $sort);

        return $builder->select()->toArray();
    }

    public function countGroupByDate($conditions, $sort, $dateColumn = 'pay_time')
    {
        if (!$this->isDateColumnAllow($dateColumn)) {
            throw $this->createInvalidArgumentException('dateColumn is not allowed');
        }

        if (!in_array(strtoupper($sort), array('ASC', 'DESC'), true)) {
            throw $this->createInvalidArgumentException("SQL order by direction is only allowed `ASC`, `DESC`, but you give `{$sort}`.");
        }

        $builder = $this->createQueryBuilder($conditions)
            ->field("count(id) as count ,from_unixtime({$dateColumn},'%Y-%m-%d') date")
            ->group("date", $sort);

        return $builder->select()->toArray();
    }

    private function isDateColumnAllow($column)
    {
        $whiteList = $this->dateColumnWhiteList();

        if (in_array($column, $whiteList)) {
            return true;
        }

        return false;
    }

    private function isSumColumnAllow($column)
    {
        $whiteList = $this->sumColumnWhiteList();

        if (in_array($column, $whiteList)) {
            return true;
        }

        return false;
    }

    private function sumColumnWhiteList()
    {
        return array('amount', 'pay_amount', 'price_amount');
    }

    private function dateColumnWhiteList()
    {
        return array('pay_time');
    }

    protected function validateFields(&$fields, $scene = '')
    {
        $validate = new OrderValidate();

        if (!$validate->scene($scene)->check($fields)) {
            throw $this->createInvalidArgumentException($validate->getError());
        }
    }

    public function declares()
    {
        return array(
            'orderBys' => [
                'id',
                'created_time',
                'updated_time',
            ],
            'conditions' => [
                'and' => [
                    ['id', 'in', 'ids'],
                    ['sn', '=', 'sn'],
                    ['user_id', '=', 'user_id'],
                    ['payment', '=', 'payment'],
                    ['created_time', '<', 'created_time_LT'],
                    ['pay_time', '<', 'pay_time_LT'],
                    ['pay_time', '>', 'pay_time_GT'],
                    ['pay_amount', '>', 'pay_amount_GT'],
                    ['price_amount', '>', 'price_amount_GT'],
                    ['source', '=', 'source'],
                    ['status', '=', 'status'],
                    ['status', 'IN', 'statuses'],
                    ['seller_id', '=', 'seller_id'],
                    ['created_time', '>=', 'start_time'],
                    ['created_time', '<=', 'end_time'],
                    ['title', 'LIKE', 'title_like'],
                    ['updated_time', '>=', ' updated_time_GE'],
                    ['refund_deadline', '<', 'refund_deadline_LT'],
                    ['invoice_sn', '=', 'invoice_sn'],
                ]
            ]
        );
    }
}
