<?php

namespace app\payment\model;

use app\common\model\DefaultModel;

class PayTradeModel extends DefaultModel
{
    protected $table = 'pay_trade';

    protected $pk = 'id';

    protected $createTime = 'created_time';

    protected $updateTime = 'updated_time';

    protected $autoWriteTimestamp = true;

    protected $type = [
        'platform_created_result' => 'json',
        'notify_data' => 'json',
        'platform_created_params' => 'json'
    ];

    public function getTrade($id, $isLock = false)
    {
        $builder = self::where('id', $id);

        if ($isLock) {
            $builder->lock(true);
        }

        $result = $builder->find();

        if (empty($result)) {
            return [];
        }

        return $result->toArray();
    }

    public function getByUserId($userId)
    {
        $result = self::where('user_id', $userId)->find();

        if (empty($result)) {
            return [];
        }

        return $result->toArray();
    }

    /**
     * @desc 根据订单号与支付平台查询
     * @param $orderSn
     * @param $platform
     * @return mixed
     */
    public function getByOrderSnAndPlatform($orderSn, $platform)
    {
        $result = self::where(['order_sn' => $orderSn, 'platform' => $platform])->find();

        if (empty($result)) {
            return [];
        }

        return $result->toArray();
    }

    /**
     * @desc 根据订单号查询多条数据
     * @param $orderSns
     * @return array
     */
    public function findByOrderSns(array $orderSns)
    {
        return self::where('order_sn', 'in', $orderSns)->select()->toArray();
    }

    /**
     * @desc 根据订单号查询多条
     * @param $orderSn
     * @return array
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\ModelNotFoundException
     * @throws \think\exception\DbException
     */
    public function findByOrderSn($orderSn)
    {
        return self::where('order_sn', $orderSn)->select()->toArray();
    }

    /**
     * @desc 根据交易号查询
     * @param $sn
     * @return array
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\ModelNotFoundException
     * @throws \think\exception\DbException
     */
    public function getByTradeSn($sn)
    {
        $result = self::where('trade_sn', $sn)->find();

        if (empty($result)) {
            return [];
        }

        return $result->toArray();
    }

    /**
     * @desc 根据交易号查询多条数据
     * @param $sns
     * @return array
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\ModelNotFoundException
     * @throws \think\exception\DbException
     */
    public function findByTradeSns($sns)
    {
        return self::where('trade_sn', 'in', $sns)->select()->toArray();
    }

    /**
     * @desc 根据第三方生成的交易号查询
     * @param $platformSn
     * @return array
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\ModelNotFoundException
     * @throws \think\exception\DbException
     */
    public function getByPlatformSn($platformSn)
    {
        $result = self::where('platform_sn', $platformSn)->find();

        if (empty($result)) {
            return [];
        }

        return $result->toArray();
    }

    public function declares()
    {
        return array(
            'conditions' => array(
                'and' => array(
                    ['order_sn', 'in', 'orderSns']
                )
            ),
        );
    }
}