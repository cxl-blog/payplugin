<?php
namespace app\payment\model\order\facade;

use app\common\model\system\log\LogModel;
use app\index\common\MathToolkit;
use app\payment\model\order\OrderModel;

abstract class Product
{
    /**
     * 商品ID
     *
     * @var int
     */
    public $targetId;

    /**
     * 商品类型
     *
     * @var string
     */
    public $targetType;

    /**
     * 商品名称
     *
     * @var string
     */
    public $title;

    /**
     * 商品原价
     *
     * @var float
     */
    public $originPrice;

    /**
     * 促销价格
     *
     * @var float
     */
    public $promotionPrice = 0;

    /**
     * 可使用的折扣
     *
     * @var array
     */
    public $availableDeducts = array();

    /**
     * 使用到的折扣
     *
     * @var array
     */
    public $pickedDeducts = array();

    /**
     * 返回的链接
     *
     * @var string
     */
    public $backUrl = '';

    /**
     * 成功支付返回链接
     *
     * @var string
     */
    public $successUrl = '';

    /**
     * 最大虚拟币抵扣百分比
     *
     * @var int
     */
    public $maxRate = 100;

    /**
     * 商品数量
     *
     * @var int
     */
    public $num = 1;

    /**
     * 商品单位
     *
     * @var string
     */
    public $unit = '';

    /**
     * 是否可以使用优惠券
     *
     * @var bool
     */
    public $couponEnable = true;

    /**
     * 封面
     *
     * @var array
     */
    public $cover = array();

    const PRODUCT_VALIDATE_FAIL = '20007';

    abstract public function init(array $params);

    abstract public function validate();

    //是否折扣商品
    public function setAvailableDeduct($params = array())
    {
    }

    // 打折处理
    public function setPickedDeduct($params)
    {

    }

    public function getPayablePrice()
    {
        $payablePrice = $this->originPrice;
        foreach ($this->pickedDeducts as $deduct) {
            $payablePrice -= $deduct['deduct_amount'];
        }

        return $payablePrice > 0 ? $payablePrice : 0;
    }

    public function getMaxCoinAmount()
    {
        return round(($this->maxRate / 100) * (new Currency())->convertToCoin($this->originPrice), 2);
    }

    protected function smsCallback($orderItem, $targetName)
    {
//        try {
//            $smsType = 'sms_'.$this->targetType.'_buy_notify';
//
//            if ($this->getSmsService()->isOpen($smsType)) {
//                $userId = $orderItem['user_id'];
//                $parameters = array();
//                $parameters['order_title'] = '购买'.$targetName.'-'.$orderItem['title'];
//                $parameters['order_title'] = StringToolkit::cutter($parameters['order_title'], 20, 15, 4);
//                $price = MathToolkit::simple($orderItem['order']['pay_amount'], 0.01);
//                $parameters['totalPrice'] = $price.'元';
//
//                $description = $parameters['order_title'].'成功回执';
//
//                $this->getSmsService()->smsSend($smsType, array($userId), $description, $parameters);
//            }
//        } catch (\Exception $e) {
//            $this->getLogService()->error(AppLoggerConstant::SMS, 'sms_'.$this->targetType.'_buy_notify', "发送短信通知失败:userId:{$orderItem['user_id']}, targetType:{$this->targetType}, targetId:{$this->targetId}", array('error' => $e->getMessage()));
//        }
    }

    protected function updateMemberRecordByRefundItem($orderItem)
    {
//        $orderRefund = $this->getOrderRefundService()->getOrderRefundById($orderItem['refund_id']);
//        $record = array(
//            'reason' => $orderRefund['reason'],
//            'refund_id' => $orderRefund['id'],
//            'reason_type' => 'refund',
//        );
//
//        $this->getMemberOperationService()->updateRefundInfoByOrderId($orderRefund['order_id'], $record);
    }

    public function getCreateExtra()
    {
        return array();
    }

    public function getSnapShot()
    {
        return array();
    }

    /**
     * @return LogModel
     */
    protected function getLogService()
    {
        return model('common/system/log/LogModel');
    }

    /**
     * @return OrderModel
     */
    protected function getOrderModel()
    {
        return new OrderModel();
    }

    protected function getCurrency()
    {
        return new Currency();
    }
}