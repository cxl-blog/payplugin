<?php

namespace app\payment\model;

use app\common\model\DefaultModel;

class PaySecurityAnswerModel extends DefaultModel
{
    protected $table = 'pay_security_answer';

    protected $pk = 'id';

    protected $createTime = 'created_time';

    protected $updateTime = 'updated_time';

    protected $autoWriteTimestamp = true;

    public function findByUserId($userId)
    {
        return self::where('user_id', $userId)->select()->toArray();
    }

    public function getSecurityAnswerByUserIdAndQuestionKey($userId, $questionKey)
    {
        $result = self::where(['user_id' => $userId, 'question_key' => $questionKey])->find();

        if (empty($result)) {
            return [];
        }

        return $result->toArray();
    }

    public function declares()
    {
        // TODO: Implement declares() method.
    }
}