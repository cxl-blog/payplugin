<?php

namespace app\payment\model;

use app\common\model\DefaultModel;

class PayAccountModel extends DefaultModel
{
    protected $table = 'pay_account';

    protected $pk = 'id';

    protected $createTime = 'created_time';

    protected $updateTime = 'updated_time';

    protected $autoWriteTimestamp = true;

    public function getByUserId($userId)
    {
        $result = self::where('user_id', $userId)->find();

        if (empty($result)) {
            return [];
        }

        return $result->toArray();
    }

    public function declares()
    {
        // TODO: Implement declares() method.
    }
}