<?php

namespace app\payment\model;

use app\common\model\DefaultModel;

class PayCashFlowModel extends DefaultModel
{
    protected $table = 'pay_cashflow';

    protected $pk = 'id';

    protected $createTime = 'created_time';

    protected $autoWriteTimestamp = true;

    public function findByTradeSn($sn)
    {
        return self::where('trade_sn', $sn)->select()->toArray();
    }

    /**
     * @param $column
     * @param $conditions
     * @return bool|string
     * @throws \RuntimeException
     */
    public function sumColumnByConditions($column, $conditions)
    {
        if (!$this->isSumColumnAllow($column)) {
            throw new \RuntimeException('column is not allowed');
        }
        $builder = $this->createQueryBuilder($conditions)
            ->sum($column);
        return $builder;
    }

    public function countUsersByConditions($conditions)
    {
        $builder = $this->createQueryBuilder($conditions)
            ->count("distinct user_id");
        return $builder;
    }

    public function sumAmountGroupByUserId($conditions)
    {
        $builder = $this->createQueryBuilder($conditions)
            ->field("sum(amount) as amount, user_id")
            ->group('user_id');

        return $builder->select()->toArray();
    }

    private function sumColumnWhiteList()
    {
        return array('amount');
    }

    protected function isSumColumnAllow($column)
    {
        $whiteList = $this->sumColumnWhiteList();

        if (in_array($column, $whiteList)) {
            return true;
        }
        return false;
    }

    public function declares()
    {
        return array(
            'orderBys' => array(
                'created_time',
            ),
            'conditions' => array(
                'and' => array(
                    ['id', '=', 'id'],
                    ['sn', '=', 'sn'],
                    ['action', '=', 'action'],
                    ['user_id', '=', 'userId'],
                    ['user_id', 'in', 'userIds'],
                    ['buyer_id', '=', 'buyerId'],
                    ['buyer_id', 'in', 'buyerIds'],
                    ['type', '=', 'type'],
                    ['title', 'like', 'titleLike'],
                    ['amount', '>', 'amount_GT'],
                    ['amount', '>=', 'amount_GE'],
                    ['amount', '<', 'amount_LT'],
                    ['amount', '<=', 'amount_LE'],
                    ['currency', '=', 'currency'],
                    ['order_sn', '=', 'orderSn'],
                    ['trade_sn', '=', 'tradeSn'],
                    ['platform', '=', 'platform'],
                    ['amount_type', '=', 'amountType'],
                    ['created_time', '>', 'createdTime_GT'],
                    ['created_time', '>=', 'createdTime_GE'],
                    ['created_time', '<', 'createdTime_LT'],
                    ['created_time', '<=', 'createdTime_LE'],
                )
            )
        );
    }
}