<?php

namespace app\payment\service;

use app\common\model\system\targetLog\TargetLogModel;
use app\common\model\system\targetLog\TargetLogService;
use app\common\util\Lock;
use app\index\common\ArrayToolkit;
use app\index\component\exception\ModelException;
use app\payment\model\PayAccountModel;
use app\payment\model\PayTradeModel;
use app\payment\status\PaidStatus;
use app\payment\status\PayingStatus;
use think\Container;
use think\Db;
use think\exception\HttpException;

class PayServiceImpl
{
    public function createTrade($data, $createPlatformTrade = true)
    {
        $data = ArrayToolkit::parts($data, array(
            'goods_title',
            'goods_detail',
            'attach',
            'order_sn',
            'amount',
            'coin_amount',
            'notify_url',
            'return_url',
            'show_url',
            'create_ip',
            'platform_type',
            'platform',
            'open_id',
            'device_info',
            'seller_id',
            'user_id',
            'type',
            'rate',
            'app_pay',
        ));

        if ('recharge' == $data['type']) {
            return $this->createRechargeTrade($data, $createPlatformTrade);
        } elseif ('purchase' == $data['type']) {
            return $this->createPurchaseTrade($data, $createPlatformTrade);
        } else {
            throw new \InvalidArgumentException("can't create the type of {$data['type']} trade");
        }
    }

    protected function createPurchaseTrade($data, $createPlatformTrade)
    {
        try {
            Db::startTrans();

            $tradeData = $this->createPayTrade($data);

            if ($tradeData['cash_amount'] > 0 && $createPlatformTrade) {
                $trade = $this->createPaymentPlatformTrade($data, $tradeData);
            }

            $trade = $this->getPayTradeDao()->get($tradeData['id']);
            Db::commit();
        } catch (\Exception $e) {
            Db::rollback();
            throw $e;
        }

        return $trade;
    }

    protected function createRechargeTrade($data, $createPlatformTrade)
    {
        try {
            Db::startTrans();
            $trade = $this->createPayTrade($data);

            if ($createPlatformTrade) {
                $trade = $this->createPaymentPlatformTrade($data, $trade);
            }

            Db::commit();
        } catch (\Exception $e) {
            Db::rollback();
            throw $e;
        }

        return $trade;
    }

    public function getTradeByTradeSn($tradeSn)
    {
        return $this->getPayTradeDao()->getByTradeSn($tradeSn);
    }

    public function findTradesByTradeSn($tradeSns)
    {
        return $this->getPayTradeDao()->findByTradeSns($tradeSns);
    }

    public function queryTradeFromPlatform($tradeSn)
    {
        $trade = $this->getPayTradeDao()->getByTradeSn($tradeSn);
        $result = $this->getPayment($trade['platform'])->queryTrade($tradeSn);

        if (PaidStatus::NAME != $trade['status'] && !empty($result)) {
            $paidTrade = $this->updateTradeToPaidAndTransferAmount($result);
            if ($paidTrade) {
                $paidTrade['platform_trade'] = $result;

                return $paidTrade;
            }
        }

        $trade['platform_trade'] = $result;

        return $trade;
    }

    public function findTradesByOrderSns($orderSns)
    {
        return $this->getPayTradeDao()->findByOrderSns($orderSns);
    }

    public function closeTradesByOrderSn($orderSn, $excludeTradeSns = array())
    {
        $trades = $this->getPayTradeDao()->findByOrderSn($orderSn);
        if (empty($trades)) {
            return;
        }

        foreach ($trades as $trade) {
            if (in_array($trade['trade_sn'], $excludeTradeSns)) {
                continue;
            }

            $trade = $this->getTradeContext($trade['id'])->closing();
            if ($this->isCloseByPayment()) {
                $this->closeByPayment($trade);
            }

            $data = array('sn' => $trade['trade_sn']);
            $this->notifyClosed($data);
        }
    }

    public function notifyPaid($payment, $data)
    {
        list($data, $result) = $this->getPayment($payment)->converterNotify($data);
        $this->getTargetlogService()->log(TargetlogService::INFO, 'trade.paid_notify', $data['trade_sn'], "收到第三方支付平台{$payment}的通知，交易号{$data['trade_sn']}，支付状态{$data['status']}", $data);

        $this->updateTradeToPaidAndTransferAmount($data);

        return $result;
    }

    public function rechargeByIap($data)
    {
        list($data, $result) = $this->getPayment('iap')->converterNotify($data);

        if ('failure' == $result) {
            throw new \Exception($data['msg']);
        }

        $platformSn = $data['cash_flow'];
        $lockKey = "recharge_by_iap_{$platformSn}";
        $lock = new Lock($lockKey);
        $lock->get($lockKey);

        $trade = $this->getTradeByPlatformSn($platformSn);

        if (!empty($trade) && 'iap' == $trade['platform']) {
            return $trade;
        }

        $trade = array(
            'goods_title' => '充值',
            'order_sn' => '',
            'platform' => 'iap',
            'platform_type' => '',
            'amount' => $data['pay_amount'],
            'user_id' => $data['attach']['user_id'],
            'type' => 'recharge',
        );
        $trade = $this->createPayTrade($trade);

        $data = array(
            'paid_time' => strtotime($data['paid_time']),
            'cash_flow' => $data['cash_flow'],
            'cash_type' => 'CNY',
            'trade_sn' => $trade['trade_sn'],
            'status' => 'paid',
            'pay_amount' => $data['pay_amount'],
        );
        $this->updateTradeToPaidAndTransferAmount($data);
        $trade = $this->getPayTradeDao()->get($trade['id']);

        $lock->release($lockKey);

        return $trade;
    }

    protected function isCloseByPayment()
    {
        return empty(config('payment.final_options')['closed_by_notify']) ? false : config('payment.final_options')['closed_by_notify'];
    }

    protected function closeByPayment($data)
    {
        $response = $this->getPayment($data['platform'])->closeTrade($data);
        if (!empty($response) && !$response->isSuccessful()) {
            $failData = $response->getMessage();
            $this->getTargetlogService()->log(TargetlogService::INFO, 'trade.close_failed', $data['trade_sn'], "交易号{$data['trade_sn']}关闭失败,{$failData},(order_sn:{$data['order_sn']})", $data);
        } else {
            $this->getTargetlogService()->log(TargetlogService::INFO, 'trade.close', $data['trade_sn'], "交易号{$data['trade_sn']}关闭成功。(order_sn:{$data['order_sn']})", $data);
        }

        return $response;
    }

    protected function updateTradeToPaidAndTransferAmount($data)
    {
        if ('paid' == $data['status']) {
            $trade = $this->getPayTradeDao()->getByTradeSn($data['trade_sn']);

            if (empty($trade)) {
                $this->getTargetlogService()->log(TargetLogService::INFO, 'trade.not_found', $data['trade_sn'], "交易号{$data['trade_sn']}不存在", $data);

                return $trade;
            }

            try {
                Db::startTrans();
                $trade = $this->getPayTradeDao()->getTrade($trade['id'], true);

                if (PayingStatus::NAME != $trade['status']) {
                    $this->getTargetlogService()->log(TargetlogService::INFO, 'trade.is_not_paying', $data['trade_sn'], "交易号{$data['trade_sn']}状态不正确，状态为：{$trade['status']}", $data);
                    Db::commit();

                    return $trade;
                }

                if ($trade['cash_amount'] != $data['pay_amount']) {
                    $this->getTargetlogService()->log(TargetlogService::INFO, 'trade.pay_amount.mismatch', $data['trade_sn'], "{$data['trade_sn']}实际支付的价格{$data['pay_amount']}和交易记录价格{$trade['cash_amount']}不匹配，状态为：{$trade['status']}", $data);
                }

                $trade = $this->updateTradeToPaid($trade['id'], $data);
                if ('purchase' == $trade['type']) {
                    $this->closeTradesByOrderSn($trade['order_sn'], array($trade['trade_sn']));
                }

                $this->getTargetlogService()->log(TargetlogService::INFO, 'trade.paid', $data['trade_sn'], "交易号{$data['trade_sn']}，账目流水处理成功", $data);
                Db::commit();
            } catch (\Exception $e) {
                $this->getTargetlogService()->log(TargetlogService::INFO, 'pay.error', $data['trade_sn'], "交易号{$data['trade_sn']}处理失败, {$e->getMessage()}", $data);
                Db::rollback();

                throw $e;
            }

            return $trade;
        }

        return $this->getPayTradeDao()->getByTradeSn($data['trade_sn']);
    }

    public function searchTrades($conditions, $orderBy, $start, $limit)
    {
        return $this->getPayTradeDao()->search($conditions, $orderBy, $start, $limit);
    }

    protected function updateTradeToPaid($tradeId, $data)
    {
        $updatedFields = array(
            'status' => $data['status'],
            'pay_time' => $data['paid_time'],
            'platform_sn' => $data['cash_flow'],
            'notify_data' => $data,
            'currency' => $data['cash_type'],
        );

        return $this->getPayTradeDao()->update($updatedFields, ['id' => $tradeId])->get($tradeId)->toArray();
    }

    public function findEnabledPayments()
    {
        return config('payment.platforms');
    }

    public function notifyClosed($data)
    {
        $trade = $this->getPayTradeDao()->getByTradeSn($data['sn']);

        return $this->getTradeContext($trade['id'])->closed();
    }

    public function applyRefundByTradeSn($tradeSn, $data = array())
    {
        $trade = $this->getPayTradeDao()->getByTradeSn($tradeSn);
        if (in_array($trade['status'], array('refunding', 'refunded'))) {
            return $trade;
        }

        if ('paid' != $trade['status']) {
            throw new AccessDeniedException('can not refund, because the trade is not paid');
        }

        if ($this->isRefundByPayment()) {
            return $this->refundPlatformTrade($trade);
        }

        $trade = $this->updateTradeToRefunded($tradeSn, $data);

        return $trade;
    }

    protected function isRefundByPayment()
    {
        return empty($this->biz['payment.final_options']['refunded_by_notify']) ? false : $this->biz['payment.final_options']['refunded_by_notify'];
    }

    protected function refundPlatformTrade($trade)
    {
        $paymentGateway = $this->getPayment($trade['platform']);
        $response = $paymentGateway->applyRefund($trade);

        if (!$response->isSuccessful()) {
            return $trade;
        }

        $trade = $this->getPayTradeDao()->update(array(
            'status' => 'refunding',
            'apply_refund_time' => time(),
        ), ['id' => $trade['id']]);
        $this->dispatch('payment_trade.refunding', $trade);

        return $trade;
    }

    public function notifyRefunded($payment, $data)
    {
        $paymentGateway = $this->getPayment($payment);
        list($result, $response) = $paymentGateway->converterRefundNotify($data);
        $tradeSn = $result['trade_sn'];

        $this->updateTradeToRefunded($tradeSn, $data);

        return $response;
    }

    protected function updateTradeToRefunded($tradeSn, $data)
    {
        $lockKey = "payment_trade_refunded_{$tradeSn}";
        $lock = new Lock($lockKey);
        $lock->get($lockKey);

        $trade = $this->getPayTradeDao()->getByTradeSn($tradeSn);
        if (RefundedStatus::NAME == $trade['status']) {
            return $trade;
        }

        $trade = $this->getTradeContext($trade['id'])->refunded($data);
        $lock->release($lockKey);

        return $trade;
    }

    protected function validateLogin()
    {
        if (empty($this->biz['user']['id'])) {
            throw new AccessDeniedException('user is not login.');
        }
    }

    protected function createPayTrade($data)
    {
        $rate = $this->getDefaultCoinRate();

        if (empty($data['amount'])) {
            throw new HttpException(401, '交易订单金额不能为空');
        }

        //待测
        $trade = array(
            'title' => $data['goods_title'],
            'trade_sn' => $this->generateSn(),
            'order_sn' => !empty($data['order_sn']) ? $data['order_sn'] : '',
            'platform' => $data['platform'],
            'platform_type' => $data['platform_type'],
            'price_type' => $this->getCurrencyType(),
            'amount' => $data['amount'],
            'rate' => $this->getDefaultCoinRate(),
            'seller_id' => empty($data['seller_id']) ? 0 : $data['seller_id'],
            'user_id' => current_user()['id'],
            'status' => 'paying',
        );

        if (!empty($data['type'])) {
            $trade['type'] = $data['type'];
        }

        if ('money' == $trade['price_type']) {
            $amount = round($trade['amount'] * $trade['rate']);
            $trade['cash_amount'] = floor($amount) / $trade['rate'];
        }

        if (0 == $trade['cash_amount']) {
            $trade['platform'] = 'none';
            $trade['platform_type'] = '';
        }

        return $this->getPayTradeDao()->create($trade, true)->getData();
    }

    protected function generateSn($prefix = '')
    {
        return $prefix . date('YmdHis', time()) . mt_rand(10000, 99999);
    }

    /**
     * @return TargetLogModel
     */
    protected function getTargetlogService()
    {
        return model('common/system/targetLog/TargetLogModel');
    }

    /**
     * @return PayTradeModel
     */
    protected function getPayTradeDao()
    {
        return new PayTradeModel();
    }

    /**
     * @return PayAccountServiceImpl
     */
    protected function getAccountService()
    {
        return new PayAccountServiceImpl();
    }

    protected function getDefaultCoinRate()
    {
        $options = config('payment.final_options');

        return empty($options['coin_rate']) ? 1 : $options['coin_rate'];
    }

    protected function getGoodsTitle()
    {
        $options = config('payment.final_options');

        return empty($options['goods_title']) ? '' : mb_substr($options['goods_title'], 0, 30, 'utf-8');
    }

    protected function getCurrencyType()
    {
        return 'money';
    }

    protected function getPayment($payment)
    {
        return config('payment.' . $payment);
    }

    protected function createPaymentPlatformTrade($data, $trade)
    {
        $data['trade_sn'] = $trade['trade_sn'];
        unset($data['user_id']);
        unset($data['seller_id']);
        $data['amount'] = $trade['cash_amount'];
        $data['platform_type'] = $trade['platform_type'];
        $data['platform'] = $trade['platform'];

        if ($title = $this->getGoodsTitle()) {
            $data['goods_title'] = $title;
        }

        $result = $this->getPayment($data['platform'])->createTrade($data);

        return $this->getPayTradeDao()->update(array(
            'platform_created_result' => $result,
            'platform_created_params' => $data,
        ), ['id' => $trade['id']]);
    }

    public function getCreateTradeResultByTradeSnFromPlatform($tradeSn)
    {
        $trade = $this->getPayTradeDao()->getByTradeSn($tradeSn);

        $result = $this->getPayment($trade['platform'])->createTrade($trade['platform_created_params']);

        $this->getPayTradeDao()->update(array(
            'platform_created_result' => $result,
        ), ['id' => $trade['id']]);

        return $result;
    }

    public function getTradeByPlatformSn($platformSn)
    {
        return $this->getPayTradeDao()->getByPlatformSn($platformSn);
    }

    protected function getTradeContext($id)
    {
        $tradeContext = config('payment.payment_trade_context');

        $trade = $this->getPayTradeDao()->get($id);
        if (empty($trade)) {
            throw new \RuntimeException("trade #{$trade['id']} is not found");
        }

        $tradeContext->setPayTrade($trade);

        return $tradeContext;
    }
}