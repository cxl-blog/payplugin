<?php

use Symfony\Component\Filesystem\Filesystem;

abstract class BaseInstallScript
{
    protected $meta;

    protected $installMode = 'app_store';

    public function __construct()
    {
        $this->meta = json_decode(file_get_contents(__DIR__ . '/../plugin.json'), true);
    }

    abstract public function install();

    public function execute()
    {
        try {
            \think\Db::startTrans();
            $this->install();
            $this->installAssets();
            \think\Db::commit();
        } catch (\Exception $e) {
            \think\Db::rollback();
            throw $e;
        }
    }

    public function setInstallMode($mode)
    {
        if (!in_array($mode, array('app_store', 'command'))) {
            throw new \RuntimeException("$mode is not validate install mode.");
        }

        $this->installMode = $mode;
    }

    protected function installAssets()
    {
        $code = $this->meta['code'];

        $rootDir = app()->getRootPath();

        $originDir = "{$rootDir}/plugins/{$code}Plugin/static-dist";
        if (!is_dir($originDir)) {
            return false;
        }

        $targetDir = "{$rootDir}/publish/static-dist/".strtolower($code).'plugin';

        $filesystem = new Filesystem();
        if ($filesystem->exists($targetDir)) {
            $filesystem->remove($targetDir);
        }

        if ($this->installMode == 'command') {
            $filesystem->symlink($originDir, $targetDir, true);
        } else {
            $filesystem->mirror($originDir, $targetDir, null, array('override' => true, 'delete' => true));
        }
    }
}
