<?php

\think\facade\Config::set([
    'options' => null,
    'final_options' => [
        'closed_by_notify' => false,
        'refunded_by_notify' => false,
        'coin_rate' => 1,
        'goods_title' => '',
    ],
    'payment_trade_context' => new \app\payment\status\PayTradeContext(),
], 'payment');

$statusArray = array(
    '\app\payment\status\ClosedStatus',
    '\app\payment\status\PayingStatus',
    '\app\payment\status\ClosingStatus',
    '\app\payment\status\PaidStatus',
    '\app\payment\status\RefundingStatus',
    '\app\payment\status\RefundedStatus',
);

foreach ($statusArray as $status) {
    \think\facade\Config::set('payment.payment_trade_status_' . $status::NAME, new $status());
}

$paymentDefaultPlatforms = array(
    'wechat' => array(
        'class' => '\app\payment\payment\WechatGateway',
        'appid' => 'wx7391b67bed8a358e',
        'mch_id' => '1522695391',
        'key' => 'CYmXo6qumPXMkz62QsNk5EC5a1Tpk9eG',
        'cert_path' => '',
        'key_path' => '',
    ),
    'alipay' => array(
        'class' => '\app\payment\payment\AlipayGateway',
        'seller_email' => '',
        'partner' => '',
        'key' => '',
    ),
    'wechat_app' => array(
        'class' => '\app\payment\payment\WeChatAppGateway',
        'appid' => '',
        'mch_id' => '',
        'key' => '',
        'cert_path' => '',
        'key_path' => '',
    ),
    'union' => array(
        'class' => '\app\payment\payment\UnionGateway',
        'merId' => '',
        'certId' => '',
        'privateKey' => ''
    )
);

\think\facade\Config::set([
    'platforms' => $paymentDefaultPlatforms,
    'wechat' => new \app\payment\payment\WeChatAppGateway(),
    'alipy' => new \app\payment\payment\AlipayGateway(),
    'wechat_app' => new \app\payment\payment\WeChatAppGateway(),
], 'payment');

